// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;

//imported openzeppelin packages.
import "./HuMeritRoyaltyModule.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

/* Contract inheritance:
    - Ownable (only cotract owner can call the method)
    - ERC721URIStorage: An Openzeppelin full ERC-721 corntract which has external Uri for NFTs.
        - tokenURI(uint256 tokenId)
        - _setTokenURI(uint256 tokenId, string memory _tokenURI)
    - IERC721Receiver: To declare this contract can recieve and handle NFT transfers
        - onERC721Received(address operator, address from, uint256 tokenId, bytes calldata data)
    - ERC721Enumerable: Adds enumerability of all the token ids in the contract as well as all token ids owned by each account.
        - tokenOfOwnerByIndex(address owner, uint256 index)
        - totalSupply()
        - tokenByIndex(uint256 index)
*/

/**
 * @title HuMerit NFT Contract
 * @dev This contract implements an ERC-721 token with additional features such as metadata, royalties, token shards, and pausing functionality.
 */
contract HuMerit_V02_Z is HuMeritRoyaltyModule, ERC721Enumerable, ERC721URIStorage, Pausable, Ownable, ERC721Burnable, IERC721Receiver {
    
    /**
     * @dev See {IERC721Receiver}.
     */
    function onERC721Received(address, address, uint256, bytes calldata) public pure override returns (bytes4) {
        return this.onERC721Received.selector;
    }

    /**
     * @dev Returns the base URI for NFT metadata.
     * Overrides the standard baseURI method for later use.
     * @return The base URI string.
     */
    function _baseURI() internal pure override returns (string memory) {
        // return "https://gateway.pinata.cloud/ipfs/";
        return "";
    }

    /*====================================================================================================*
     * HuMerit:
     *====================================================================================================*/
    using Counters for Counters.Counter;

    // Token id counter
    Counters.Counter private _tokenIdCounter;

    // The account which receives HuMerit fees (Currently is handled in HuMerit Web2.0 backend)
    address payable public immutable humeritFeeAccount; 

    // The fee percentage on sales (Currently is handled in HuMerit Web2.0 backend)
    uint public immutable humeritFeePercent; 

    constructor() ERC721("HuMerit NFT", "HNFT") {

        // These variables are set but not used at present
        humeritFeeAccount = payable(msg.sender);
        humeritFeePercent = 5;
    }

    // Just for test
    function isAlive() public pure returns (string memory) {
        return "I am alive...";
    }

    // Calls the standard OpenZeppelin's ERC721 token exists method
    function tokenExists(uint256 tokenId) public view returns (bool){
        return _exists(tokenId);
    }

    // Calls the standard OpenZeppelin's ERC721Pausable smart contract pause method
    function pause() public onlyOwner {
        _pause();
    }

    // Calls the standard OpenZeppelin's ERC721Pausable smart contract unpause method
    function unpause() public onlyOwner {
        _unpause();
    }

    /**
     * @dev Mints a new NFT using OpenZeppelin's ERC721 safeMint method.
     * Sets the NFT's tokenURI using the OpenZeppelin's ERC721URIStorage setTokenURI method.
     * Sets the NFT's royalty using the OpenZeppelin's ERC2981 setTokenRoyalty method.
     * @param nftOwner The address that will own the minted NFT.
     * @param newTokenURI The metadata URI for the minted NFT.
     * @param royalty The royalty percentage associated with the NFT.
     * @return The ID of the newly minted NFT.
     */
    function safeMintNFT(address nftOwner, string memory newTokenURI, uint96 royalty) external onlyOwner nonReentrant whenNotPaused returns (uint256) {
        require(royalty >= 10, "HuMerit: Royalty must be greater than 10");
        require(bytes(newTokenURI).length > 0, "HuMerit: Invalid TokenURI");
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(nftOwner, tokenId);
        _setTokenURI(tokenId, newTokenURI);
        _setTokenRoyalty(tokenId, nftOwner, royalty); // feeDenominator is 10000, so royalty = 1000 means 10%;
        return tokenId;
    }

    /**
     * @dev Sets NFT shards for a given token.
     * Accepts two seperated arrays with the same lengths for participants and their shards.
     * @param _tokenId The ID of the token for which to set shards.
     * @param totalShards The total number of shards.
     * @param participants An array of participant addresses.
     * @param shards An array of shard values for participants.
     */
    function setTokenShards(uint256 _tokenId, uint256 totalShards, address[] calldata participants, uint8[] calldata shards) public onlyOwner whenNotPaused {
        require(tokenExists(_tokenId), "HuMerit: Token Id does not exists");
        _setTokenShards(_tokenId, totalShards, participants, shards);
    }

    /**
     * @dev Burns (destroys) an NFT with a given token ID.
     * resets the NFT royalty and shards.
     * @param _tokenId The ID of the token to burn.
     */
    function _burn(uint256 _tokenId) internal whenNotPaused override(ERC721, ERC721URIStorage) {
        require(_isApprovedOrOwner(_msgSender(), _tokenId), "HuMerit: caller is not owner nor approved");
        super._burn(_tokenId);

        /* ERC2981 */
        _resetTokenRoyalty(_tokenId);
        _resetTokenShards(_tokenId);
    }

    /**
     * @dev Executes before a token transfer occurs.
     * @param from The address from which the tokens are being transferred.
     * @param to The address to which the tokens are being transferred.
     * @param firstTokenId The ID of the first token in the batch.
     * @param batchSize The size of the token batch being transferred.
     */
    function _beforeTokenTransfer(address from, address to, uint256 firstTokenId, uint256 batchSize)
        internal
        whenNotPaused
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, firstTokenId, batchSize);
    }

    /**
     * @dev Retrieves the metadata URI for a given token ID.
     * @param _tokenId The ID of the token.
     * @return The metadata URI.
     */
    function tokenURI(uint256 _tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        require(tokenExists(_tokenId), "HuMerit: Token Id does not exists");
        return super.tokenURI(_tokenId);
    }

    /**
     * @dev Returns the total count of tokens in the contract.
     * @return The total token count.
     */
    function tokenCount() public view returns (uint256)
    {
        uint256 tokenCurrentId = _tokenIdCounter.current();
        return tokenCurrentId;
    }
    
    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view override(ERC721, ERC721Enumerable, HuMeritRoyaltyModule, ERC721URIStorage) returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}