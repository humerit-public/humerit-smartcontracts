// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;

//imported openzeppelin packages.
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/common/ERC2981.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

/* Contract inheritance:
    - ERC2981
        - royaltyInfo(uint256 _tokenId, uint256 _salePrice)
        - _setTokenRoyalty(uint256 tokenId, address receiver, uint96 feeNumerator)
*/

/**
 * @title HuMeritRoyaltyModule
 * @dev Abstract contract that provides functionality for managing token shards and royalties over the ERC2981 standard.
 */
abstract contract HuMeritRoyaltyModule is ERC2981, ReentrancyGuard {
    mapping(uint256 => mapping(address => uint8)) private _tokenShardsMap;
    mapping(uint256 => address[]) private _tokenAddresses;
    mapping(uint256 => uint256) private _tokenTotalShards;

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC2981) returns (bool) {
        return super.supportsInterface(interfaceId);
    }

    /**
     * @dev Returns whether `tokenId` shards exists.
     * Token shards start existing when _setTokenShards is called (`_setTokenShards`),
     * and stop existing when they are burned (`_burn`, '_resetTokenShards').
     */
    function _tokenShardsExists(uint256 _tokenId) internal view returns (bool) {
        return _tokenTotalShards[_tokenId] > 0;
    }

    /**
     * @dev Retrieves the shard information for a given token and sale price.
     * @param _tokenId The ID of the token.
     * @param _salePrice The price at which the token was sold.
     * @return An array of shard addresses and their corresponding shard values.
     */
    function shardInfos(uint256 _tokenId, uint256 _salePrice) public view returns (address[] memory, uint[] memory) {
        require(_tokenShardsExists(_tokenId), "RoyaltyModule: Token Id does not exists");
        require(_salePrice > 0, "RoyaltyModule: Price must be greater than zero");

        uint count = _tokenAddresses[_tokenId].length;
        address[] memory shardAdresses = new address[](count);
        uint[] memory shardValues = new uint[](count);

        for (uint i=0; i < count; i++) {
            shardAdresses[i] = _tokenAddresses[_tokenId][i];
            shardValues[i] = _tokenShardsMap[_tokenId][shardAdresses[i]];
        }

        return (shardAdresses, shardValues);
    }

    /**
     * @dev Retrieves the shard percentage for a given participant and token.
     * @param _tokenId The ID of the token.
     * @param participant The address of the participant.
     * @return The shard percentage for the participant.
     */
    function participantShardPercent(uint256 _tokenId, address participant) public view returns (uint256) {
        require(_tokenShardsExists(_tokenId), "RoyaltyModule: Token Id does not exists");
        require(participant != address(0), "RoyaltyModule: invalid participant address");

        address tokenRoyaltyReciever;
        uint256 tokenRoyaltyAmount;
        uint tokenTotalShards = _tokenTotalShards[_tokenId];
        uint participantShard = _tokenShardsMap[_tokenId][participant];
        uint participantShardInPercent = (participantShard * _feeDenominator()) / tokenTotalShards;
        uint96 feeDenominator = _feeDenominator() / 100;
        (tokenRoyaltyReciever, tokenRoyaltyAmount)  = royaltyInfo(_tokenId, feeDenominator);

        participantShardInPercent = (tokenRoyaltyAmount * participantShardInPercent) / 100;
        return participantShardInPercent;
    }

    /**
     * @dev Retrieves the shard amount for a given participant and token.
     * @param _tokenId The ID of the token.
     * @param _salePrice The price at which the token was sold.
     * @param participant The address of the participant.
     * @return The shard amount for the participant.
     */
    function participantShardAmount(uint256 _tokenId, uint256 _salePrice, address participant) public view returns (uint256) {
        require(_tokenShardsExists(_tokenId), "RoyaltyModule: Token Id does not exists");
        require(participant != address(0), "RoyaltyModule: invalid participant address");
        require(_salePrice > 0, "RoyaltyModule: Price must be greater than zero");

        uint participantShardInPercent = participantShardPercent(_tokenId, participant);
        uint256 shardAmount = (participantShardInPercent * _salePrice) / _feeDenominator();
        return shardAmount;
    }

    /**
     * @dev Sets the token shards for a specific token ID.
     * @param _tokenId The ID of the token.
     * @param totalShards The total number of shards.
     * @param participants An array of participant addresses.
     * @param shards An array of shard values for participants.
     *
     * Requirements:
     *
     * - `tokenId` must be already minted.
     * - `totalShards` cannot be the zero address.
     * - `totalShards` cannot be greater than the fee denominator.
     * - 'feeDenominator' at this point fee denominator is equal to ERC2981 default fee denominator.
     */
    function _setTokenShards(uint256 _tokenId, uint256 totalShards, address[] calldata participants, uint8[] calldata shards) internal {
        require(participants.length == shards.length, "RoyaltyModule: participants and shards array sizes are not equal");
        require(totalShards > 0, "RoyaltyModule: totalShards must be greater than zero");
        require(totalShards <= _feeDenominator(), "RoyaltyModule: totalShards will exceed max value");

        _tokenTotalShards[_tokenId] = totalShards;
        _tokenAddresses[_tokenId] = participants;
        for (uint i=0; i < participants.length; i++) {
            require(participants[i] != address(0), "RoyaltyModule: Invalid participant");
            _tokenShardsMap[_tokenId][participants[i]] = shards[i];
        }
    }

    /**
     * @dev Resets the token shards for a specific token ID.
     * @param _tokenId The ID of the token.
     */
    function _resetTokenShards(uint256 _tokenId) internal {
        for (uint i=0; i < _tokenAddresses[_tokenId].length; i++) {
            delete _tokenShardsMap[_tokenId][_tokenAddresses[_tokenId][i]];
        }
    }

}