import { ethers } from "hardhat";

async function main() {
  const wallet = new ethers.Wallet(process.env.METAMASK_WALLET_PRIVATE_KEY!);
  const HuMreit_V02_Z = await ethers.getContractFactory("HuMreit_V02_Z");
  const huMreit_V02_Z = await HuMreit_V02_Z.deploy();
  await huMreit_V02_Z.waitForDeployment();
  console.log("HuMreit_V02_Z deployed. The contract address is:", huMreit_V02_Z.target);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
