import { ethers } from "hardhat";
import { anyValue } from "@nomicfoundation/hardhat-chai-matchers/withArgs";
import { expect } from "chai";

const toWei = (num) => ethers.utils.parseEther(num.toString())
const fromWei = (num) => ethers.utils.formatEther(num)

describe("Humerit V02 Smart Contract Tests", function () {

    let huMerit_V2: any;
    let deployer: any;
    let addr1: any;
    let addr2: any;
    let addrs: any;
    let feePercent = 5;
    const tokenURI_1 = '{"name": "City Lights", "description": "Lights through the buildings by Somayeh Shahmohammadi.", "image": "https://gateway.pinata.cloud/ipfs/QmY71tnmCf83WWDU1DRsyTdWed3xP5F3bCPDTN5MXqFLNM"}';
    const tokenURI_2 = "https://gateway.pinata.cloud/ipfs/QmY71tnmCf83WWDU1DRsyTdWed3xP5F3bCPDTN5MXqFLNM";

    this.beforeEach(async function () {
        // This is executed before each test
        // Deploying the smart contract
        const HuMerit_V2 = await ethers.getContractFactory("HuMerit_V02_Z");
        huMerit_V2 = await HuMerit_V2.deploy();
        await huMerit_V2.waitForDeployment();
        [deployer, addr1, addr2, ...addrs] = await ethers.getSigners();
    })

    describe("Deployment", function () {
        it("Should track name and symbol of the contract", async function () {
            // This test expects the owner variable stored in the contract to be equal
            // to our Signer's owner.
            const humName = "HuMerit NFT"
            const humSymbol = "HNFT"
            expect(await huMerit_V2.name()).to.equal(humName);
            expect(await huMerit_V2.symbol()).to.equal(humSymbol);
        });

        it("Should track feeAccount and feePercent of the contract", async function () {
            expect(await huMerit_V2.humeritFeeAccount()).to.equal(deployer.address);
            expect(await huMerit_V2.humeritFeePercent()).to.equal(feePercent);
        });
    });

    describe("Minting NFTs", function () {
        it("Should track minting reqirements and validations", async function () {
            await expect(huMerit_V2.connect(deployer).safeMintNFT(deployer.address, tokenURI_1, 9)).to.be.revertedWith('HuMerit: Royalty must be greater than 10');
            await expect(huMerit_V2.connect(addr2).safeMintNFT(addr1.address, tokenURI_1, 10)).to.be.revertedWith('Ownable: caller is not the owner');
            await expect(huMerit_V2.connect(addr2).burn(1)).to.be.revertedWith('ERC721: invalid token ID');
        })
        it("Should track each minted NFT", async function () {
            // deployer mints an nft
            expect(await huMerit_V2.balanceOf(deployer.address)).to.equal(0);
            const tx = await huMerit_V2.connect(deployer).safeMintNFT(deployer.address, tokenURI_1, 10);
            expect(await huMerit_V2.tokenCount()).to.equal(1);
            expect(await huMerit_V2.balanceOf(deployer.address)).to.equal(1);
            expect(await huMerit_V2.tokenURI(0)).to.equal(tokenURI_1);
            // deployer mints an nft for addr1
            expect(await huMerit_V2.balanceOf(addr1.address)).to.equal(0);
            await huMerit_V2.connect(deployer).safeMintNFT(addr1.address, tokenURI_2, 10)
            expect(await huMerit_V2.tokenCount()).to.equal(2);
            expect(await huMerit_V2.balanceOf(addr1.address)).to.equal(1);
            expect(await huMerit_V2.tokenURI(1)).to.equal(tokenURI_2);

            var tokenCount = await huMerit_V2.tokenCount();
            console.log("\ttokenCount is", tokenCount.toString());
        });

        it("Should track NFT is burned successfully", async function () {
            const tx1 = await huMerit_V2.connect(deployer).safeMintNFT(deployer.address, tokenURI_1, 10);
            expect(await huMerit_V2.balanceOf(deployer.address)).to.equal(1);
            const tx2 = await huMerit_V2.connect(deployer).burn(0);
            expect(await huMerit_V2.tokenCount()).to.equal(1);
            expect(await huMerit_V2.balanceOf(deployer.address)).to.equal(0);
        })

        it("Should track if user can call super methods", async function () {
            const tx1 = await huMerit_V2.connect(deployer).safeMintNFT(deployer.address, tokenURI_1, 10);
            expect(await huMerit_V2.ownerOf(0)).to.equal(deployer.address);
            const ownr = await huMerit_V2.ownerOf(0);
            console.log("\tOwnerOf 0 is", ownr.toString());
        })
    })

    describe("Royalty Module", function () {
        it("Should track IRC2981 royalty is set successfully", async function () {
            const tokenId = 0;
            const salePrice = 100;
            const royalty = 1500;
            const tx1 = await huMerit_V2.connect(deployer).safeMintNFT(deployer.address, tokenURI_1, royalty);
            const result = await huMerit_V2.royaltyInfo(tokenId, salePrice);
            const { 0: reciever, 1: amount } = result;
            console.log("\troyaltyInfo is: ", result);
            expect(reciever).to.equal(deployer.address);
            expect(amount).to.equal(15);
        })

        it("Should track RoyaltyModule shards is set successfully", async function () {
            const tokenId = 0;
            const salePrice = 100;
            const royalty = 3000;
            const totalShards = 1000;
            const participants = [deployer.address, addr1.address, addr2.address];
            const shards = [5, 25, 26];
            const tx1 = await huMerit_V2.connect(deployer).safeMintNFT(deployer.address, tokenURI_1, royalty);
            const tx2 = await huMerit_V2.connect(deployer).setTokenShards(tokenId, totalShards, participants, shards);
            const result = await huMerit_V2.shardInfos(tokenId, salePrice);
            const { 0: shardAdresses, 1: shardValues } = result;

            console.log("\tshardInfo addresses are: ", JSON.stringify(shardAdresses));
            console.log("\tshardInfo values are: ", shardValues.toString());
            expect(shardAdresses[0]).to.equal(participants[0]);
            expect(shardValues[0]).to.equal(shards[0]);

            const participantShardPercent = await huMerit_V2.participantShardPercent(tokenId, deployer.address);
            console.log("\tParticipant shard percentage of total NFT considering royalty percentage: (%i/%i)%%", participantShardPercent.toString(), 100);
            expect(participantShardPercent).to.equal((shards[0] * royalty) / totalShards);

            const participantShardAmount = await huMerit_V2.participantShardAmount(tokenId, salePrice, deployer.address);
            console.log("\tParticipant shard amount of total NFT sale price considering royalty percentage: %i", participantShardAmount.toString());
            expect(participantShardAmount).to.equal(0);
        })
    });
})