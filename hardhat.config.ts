import { HardhatUserConfig, task } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import "dotenv/config";

task("accounts", "Prints the list of accounts", async (taskArgs, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

const config: HardhatUserConfig = {
  solidity: "0.8.18",
  networks: {
    hardhat: {
      accounts: {
        // These accounts are used to deploy contracts and fund all Airnode
        // related accounts. Make sure they have more than enough ETH to
        // do this (1m ETH each).
        accountsBalance: '1000000000000000000000',
        count: 1000,
      },
    },
    localhost: {
      url: process.env.LOCAL_NETWORK_URL,
      accounts: [process.env.LOCAL_WALLET_PRIVATE_KEY!]
    },
    polygon: {
      url: process.env.POLYGON_URL,
      accounts: [process.env.METAMASK_WALLET_PRIVATE_KEY!]
    },
    mumbai: {
      url: process.env.MUMBAI_URL,
      accounts: [process.env.METAMASK_WALLET_PRIVATE_KEY!]
    }
  },
  etherscan: {
    apiKey: process.env.POLYGONSCAN_API_KEY
  },
};

export default config;
