# HuMerit, Inc.
Platform to earn rewards based on shared vision.
HuMerit is an online multi-sided software platform that enables a reciprocal relationship between artists and creators on one side, and social media users and influencers on the other. One side of this relationship will increase visibility of the artists with their targeted audience, and the second side will reward social media users for their time, and influencers for their social capital.

## Overview
This document demonstrates the HuMerit smart contract architecture and business logic.

### Architecture
-![alt text](https://gitlab.com/humerit-public/humerit-smartcontracts/-/raw/main/arch.png?ref_type=heads&inline=false)


### Smart Contracts
#### HuMerit_Vxx_Z.sol
Core NFT contract responsible for minting NFTs and setting their related informations such as metadata and royalty. This contract is also responsible for execution of the transfers.

It uses the following OpenZeppelin's libraries:
  - `ERC721`
  - `ERC721Enumerable`
  - `ERC721URIStorage`
  - `ERC721Burnable`
  - `IERC721Receiver`
  - `ERC2981`
  - `ERC165`
  - `ReentrancyGuard`
  - `Counters`
  - `Pausable`
  - `Ownable`

It inherits the following contracts
  - `HuMeritRoyaltyModule`

#### HumeritRoyaltyModule.sol
This smart contract is responsible for calculating and storing artists' royalties from NFT as well as HuMerit participants' shares from it.

## Development Documentation
Node version v18

- Install packages - `npm install`
- Compile contracts - `npx hardhat compile`
- Run tests - `npx hardhat test`
- Deploy contract - `npx hardhat run scripts/deploy.ts`

Try running some of the following tasks:

```shell
npx hardhat test
npx hardhat node
npx hardhat run scripts/deploy.ts
```
